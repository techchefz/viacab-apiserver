/* eslint-disable */
import ServerConfig from '../models/serverConfig';  //eslint-disable-line
import User from '../models/user';
import RequestTrip from "../models/trip-request";
import sendSms from "../service/smsApi";

function mobileVerify(req, res, next) {
  User.findOneAsync({ email: req.query.email })
    //eslint-disable-next-line
    .then(foundUser => {
      if (foundUser) {
        const host = req.get('host');
        if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
          if (req.query.otp == foundUser.otp) {
            User.findOneAndUpdateAsync(
              { email: req.query.email },
              { $set: { mobileVerified: true } },
              { new: true }
            ).then((updateUserObj) => { //eslint-disable-line
              if (updateUserObj) {
                const returnObj = {
                  success: true,
                  message: 'Mobile verified',
                  data: {}
                };
                returnObj.success = true;
                return res.send(returnObj);
              }
            }).error((e) => {
              const err = new APIError(`error in updating user details while login ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
              next(err);
            });
          } else {
            const returnObj = {
              success: false,
              message: 'Error Verifying Mobile',
              data: {}
            };
            return res.send(returnObj);
          }
        }
      }
    });
}

function emailVerify(req, res, next) {

  User.findOneAsync({ email: req.query.email })
    //eslint-disable-next-line
    .then(foundUser => {
      if (foundUser) {
        const host = req.get('host');
        console.log(req.protocol + ":/" + req.get('host'));
        if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
          console.log("Domain is matched. Information is from Authentic email");
          if (req.query.check === foundUser.otp) {
            User.findOneAndUpdateAsync({ email: req.query.email }, { $set: { emailVerified: true } }, { new: true }) //eslint-disable-line
              .then((updateUserObj) => { //eslint-disable-line
                if (updateUserObj) {
                  const returnObj = {
                    success: true,
                    message: 'Email verified',
                    data: {}
                  };
                  // returnObj.data.user = updateUserObj;
                  returnObj.success = true;
                  return res.send(returnObj);
                }
              })
              .error((e) => {
                const err = new APIError(`error in updating user details while login ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
                next(err);
              });
            console.log("Email is verified");
            res.end("<h1>Email is been Successfully verified</h1>");
          }
          else {
            console.log("Email is not verified");
            res.end("<h1>Bad Request</h1>");
          }
        }
      }
    });
}

function isVerified(req, res, next) {
  User.findOneAsync({ email: req.headers.email })
    .then((foundUser) => {
      const returnObj = {
        message: "",
        success: true,
        data: {}
      }
      if (foundUser.mobileVerified == true) {
        returnObj.message = "User Found And Is Verified";
        returnObj.success = true;
        res.send(returnObj);
      } else {
        returnObj.message = "User Found But Not Verified";
        returnObj.success = false;
        res.send(returnObj);
      }
    })
}

function rideVerify(req, res, next) {
  const returnObj = {
    message: "",
    success: false,
    data: {}
  }
  RequestTrip.findById({ _id: req.headers.tripid })
    .then((idfound) => {
      if (idfound.tripOtp == req.headers.rideotp) {
        returnObj.message = "otp verified";
        returnObj.success = true;
        res.send(returnObj);
      } else {
        returnObj.message = "otp not verified";
        returnObj.success = false;
        res.send(returnObj);
      }
    })
}

function reSend(req, res) {
  const otpValue = Math.floor(100000 + Math.random() * 900000);
  User.findOneAndUpdateAsync({ email: req.headers.email }, { $set: { otp: otpValue } }, { new: true })
    .then((foundUser) => {
      const returnObj = {
        success: true,
        message: "",
        data: {}
      }
      if (foundUser) {
        returnObj.success = true;
        returnObj.message = "OTP Sent";
        returnObj.data.phoneNo = foundUser.phoneNo;
        if (req.headers.request_type == "ResetPassword") {
          sendSms(foundUser._id, "Otp To Reset Password Don't Share" + foundUser.otp, foundUser.phoneNo);
        } else {
          sendSms(foundUser._id, "Otp for Mobile Verification " + foundUser.otp, foundUser.phoneNo);
        }
        res.send(returnObj)
      } else {
        returnObj.success = false;
        returnObj.message = "OTP Not Sent";
        res.send(returnObj)
      }
    })
}

export default {
  mobileVerify,
  emailVerify,
  isVerified,
  rideVerify,
  reSend
};
