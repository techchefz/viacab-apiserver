import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import QuerySchema from "../models/query";

function newQuery(req, res, next) {
    console.log(req.body)
    const newQuery = new QuerySchema({
        userId: req.body._id,
        queryMessage: req.body.message,
        querySubject: req.body.subject
    });
    newQuery.saveAsync();
    const returnObj = {
        success: true,
        message: 'Query Successfully Submitted',
        data: newQuery
    };
    res.send(returnObj);
}

function getAllQuery(req, res, next) {
    QuerySchema.find({ userId: req.headers._id })
        .sort({ submittedOn: -1 })
        .populate('userId')
        .then((foundQuery) => {
            const returnObj = {
                success: false,
                message: 'No Query Found',
                data: {}
            };
            if (foundQuery.length == 0) {
                res.send(returnObj);
            } else {
                returnObj.success = true;
                returnObj.message = "Found Query`s";
                returnObj.data = foundQuery;
                res.send(returnObj);
            }
        });
}


function getadminQuery(req, res, next) {
    QuerySchema.find()
        .sort({ submittedOn: -1 })
        .populate('userId')
        .then((foundQuery) => {
            const returnObj = {
                success: false,
                message: 'No Query Found',
                data: {}
            };
            if (foundQuery.length == 0) {
                res.send(returnObj);
            } else {
                returnObj.success = true;
                returnObj.message = "Found Query`s";
                returnObj.data = foundQuery;
                res.send(returnObj);
            }
        });
}

function changeQueryStatus(req, res, next) {
    QuerySchema.findOneAndUpdate({ _id: req.body.tripId }, { $set: { queryStatus: req.body.queryStatus } }, { new: true })
        .then((updateQuery) => {
            const returnObj = {
                success: false,
                message: 'Not Found Or Something Went Wrong!',
                data: {}
            };
            if (updateQuery) {
                returnObj.success = true;
                returnObj.message = "Stauts Changed";
                returnObj.data = updateQuery;
                res.send(returnObj);
            } else {
                res.send(returnObj);
            }
        })
}

export default {
    newQuery,
    getAllQuery,
    changeQueryStatus,
    getadminQuery
}