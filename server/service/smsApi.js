import Twilio from 'twilio';
import ServerConfig from '../models/serverConfig';
import UserSchema from '../models/user';
import axios from "axios";
function getSmsApiDetails() {
  return new Promise((resolve, reject) => {
    ServerConfig.findOneAsync({ key: 'smsConfig' })
      .then((foundDetails) => {
        resolve(foundDetails.value);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function sendSmsTwilio(userId, smsText) {
  UserSchema.findOneAsync({ _id: userId }).then((userObj) => {
    getSmsApiDetails().then((details) => {
      const twilio = new Twilio(details.accountSid, details.token);
      twilio.messages.create(
        {
          from: details.from,
          to: userObj.phoneNo,
          body: smsText
        },
        (err, result) => {
          if (err) {
            return err;
          }
          return result;
        }
      );
    });
  });
}

function sendSms(userId, smsText) {
  UserSchema.findOneAsync({ _id: userId }).then((userObj) => {
    const num = userObj.phoneNo;
    var userMobileNum = num.slice(-10)
    // axios.get(`http://103.247.98.91/API/SendMsg.aspx?uname=20171501&pass=Raytheorysms1&send=PRPSCN&dest=${userMobileNum}&msg=Hi%20:${userObj.fname},%20Welcome%20to%20CABBIGFamily%20!%20Your%20OTP%20Verification%20Code%20is%20${smsText}`)
    //   .then((resp) => {
    //     console.log('====================================');
    //     console.log(resp.statusText);
    //     console.log('====================================');
    //   }).catch((e) => console.log("ERROR" + e));
  });
}


export default sendSms;
